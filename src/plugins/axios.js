import axios from 'axios'

export default ({Vue}) => {
  Vue.prototype.$axios = axios
  axios.defaults.baseURL = 'http://localhost:8080/ru/resources/'
  Vue.prototype.$restApiResources = {
    marmitex: (parametros) => {
      return axios({
        method: 'post',
        url: 'marmitex/cria',
        data: parametros
      })
    },
    cadeirasResource: () => {
      return axios({
        method: 'get',
        url: 'sessao/cadeiras'
      })
    },
    criaSessao: (parametros) => {
      return axios({
        method: 'post',
        url: 'sessao/cria',
        data: parametros
      })
    },
    listaSessao: (idUser, start, end) => {
      let sessao = 'sessao/sessaoUser?idUser={0}&start={1}&end={2}'
      sessao = sessao.replace('{0}', idUser).replace('{1}', start).replace('{2}', end)
      return axios({
        method: 'get',
        url: sessao
      })
    }
  }
}

// this.$restApiResources.sessao(parametros);
