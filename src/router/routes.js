export default [
  {
    path: '/',
    name: 'root',
    component: () => import('layouts/private'),
    children: [
      {path: '', component: () => import('pages/index')},
      {path: 'marmitex', name: 'marmitex', component: () => import('pages/marmitex')},
      {path: 'sessao', name: 'sessao', component: () => import('pages/sessao')},
      {path: 'batata', name: 'batata', component: () => import('pages/sessoes')}
    ]
  },

  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
